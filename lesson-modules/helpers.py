__all__ = (
    'prompt',
    'input_date',
    'input_datetime',
    'print_table',
    'print_task',
)

debug = False

def is_debug():
    print(f'Debug: {debug}')

def set_debug(value):
    global debug
    debug = value

def prompt():
    pass


def input_datetime():
    pass


def input_date():
    pass


def print_table():
    pass


def print_task():
    pass


if __name__ == '__main__':
    print(__name__) # имя модуля
    print('Запускать тестирование')
