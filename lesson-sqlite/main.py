"""
Базы данных

СУБД
- MySQL (MariaDB)
- PostreSQL
- Oracle
- MS SQL Server
- SQLite3 - база данных в одном файле

SQL
-> DDL - Data Definition Language
    CREATE TABLE
-> DML - Data Manupulation Language
    INSERT INTO
    UPDATE
    DELETE
    SELECT

:memory: - БД в оперативной памяти
SQLite Browser

Алгоритм работы с БД:
1. Установить соединение с сервером БД
    1.1. Опционально (для СУБД), выбрать БД
2. Выполнение запроса:
    2.1. Получить объект курсора (опционально)
    2.2. Выполнить запрос с помощью метода execute
    2.3. Если запрос на изменение структуры или данных БД:
        2.3.1. Нужно зафиксировать изменения (опционально)
    2.4. Если запрос на получение данных (SELECT)
        2.4.1. Фактические данные нужно раз-fetch-ить:
            - fetchall()   -> получить все строки таблицы в список
            - fetchone()   -> получить одну строку из таблицы
            - fetchmany(N) -> получить N строк из таблицы
3. Закрыть соединение с сервером БД
"""

from datetime import datetime
import sqlite3

sql = '''
    CREATE TABLE IF NOT EXISTS diary (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        title TEXT NOT NULL,
        planned TIMESTAMP NOT NULL,
        description TEXT NOT NULL DEFAULT "",
        done BOOLEAN NOT NULL DEFAULT 0,
        created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
    )
'''


conn = sqlite3.connect('diary.sqlite')
cursor = conn.cursor()
cursor.execute(sql)
conn.commit()
conn.close()


sql = '''
    INSERT INTO diary (
        title, planned, description
    ) VALUES (
        ?, ?, ?
    )
'''

"""
UPDATE diary SET title=?, description=? WHERE id=?
DELETE FROM diary WHERE id=?
"""

with sqlite3.connect('diary.sqlite') as conn:
    data = (
        'Сделать ДЗ на среду',
        datetime(2020, 2, 12),
        'Обязательно',
    )
    conn.execute(sql, data)


sql = '''
    SELECT
        id, title, planned, description, done, created
    FROM
        diary
'''

"""
detect_types
1. PARSE_DECLTYPES
2. PARSE_COLNAMES (забыть)

fetchall

cursor
rows = [(1, ''), (2, '')]
result = []
for row in rows:
    result.append(row_factory(cursor, row))
return result

def conn.execute():
    cursor = conn.cursor()
    cursor.execute()
    return cursor
"""

with sqlite3.connect('diary.sqlite', detect_types=sqlite3.PARSE_DECLTYPES) as conn:
    conn.row_factory = sqlite3.Row

    cursor = conn.execute(sql)
    rows = cursor.fetchall()

    for row in rows:
        print(type(row['planned']), row['done'])









