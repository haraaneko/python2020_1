# todo: Ветвление
# expr1 or expr2 or expr3

a = 1
b = 2

if a > b:
    print('a > b')
elif a == b:
    print('a = b')
else:
    print('a < b')

"""
todo: Тернарный оператор

if a is not None:
    a = int(a)
else:
    a = 0
"""

a = None
a = int(a) if a is not None else 0


# todo: Циклы

i = 1

while i:
    if i == 10:
        break

    if not i % 2:
        print(i)

    i += 1


lst = [1, 2, 3]

for value in lst:
    print('=>', value)


for idx, value in enumerate(lst):
    print('Index:', idx, 'Value:', value)


person = {
    'name': 'Linus Torvalds',
    'age': 50,
    'is_developer': True,
    'skills': ('C++', 'Linux'),
}

for key, value in person.items():
    print(key, '=>', value)


for i in range(1, 11, 2):
    print(i)















