"""
todo: Функции
"""

# todo: Как определить функцию?

def print_hello():
    print('Hello, Python!')

# todo: Как выполнить вызов функции?

print_hello()
print_hello()
print_hello()

# todo: Как вернуть значение из функции?

def get_hello():
    return 'Hello, Python!'
    print('Никогда не выполнится')

a = get_hello()
print(a, type(a))


# todo: Зачем функции аргументы?

def get_greeting(name):
    return f'Hello, {name}! {name}'


print(get_greeting('Вася'))
print(get_greeting('Петя'))
print(get_greeting('Гарри Топор'))


def summa(a, b):
    return a + b

# todo: Как задать значение аргумента по-умолчанию?

def summa_2(a, b, c=0, d=0):
    return a + b + c + d

print(summa_2(1, 2), summa_2(1, 2, 3))


# todo: Переменное количество аргументов

def cool_summa(a, b, *numbers):
    print(type(numbers), numbers)

    result = a + b

    for i in numbers:
        result += i

    return result

print(cool_summa(b=2, a=1))
print(cool_summa(1, 2, 3, 4, 5))


def db_connect(provider, **config):
    print(provider, type(config), config)


db_connect('sqlite', filename='db.sqlite')
db_connect(provider='mysql', host='localhost', user='root')

# *args **kwargs

# todo: Как развернуть кортеж/список
# в значения позиционных аргументов при вызове

lst = [7, 8, 9, 10]
result = cool_summa(*lst)
print(f'Сумма элементов списка: {result}')
print(f'Сумма элементов списка: {cool_summa(*lst)}')

# todo: Как развернуть словарь
# в значения именованных аргументов при вызове

options = {
    'filename': 'db.sqlite',
    'provider': 'sqlite',
}

db_connect(**options)

"""
todo: Передача аргументов по значению и по ссылке
все скалярные типы данных передаются по значению (копия)
все ссылочные типы данных передаются по ссылке
"""

src = 'Python is programming language.'
lst = []

def parse(src, output=None):
    if output is None:
        output = []

    print('==>', output)

    src = src.strip('.')

    for i in src.split():
        output.append(i)

    return output

print(src, lst)
parse(src)
parse(src)
parse(src)
parse(src)
print(src, lst)


# todo: Анонимная функция

lst = [1, 2, 3, 4, 5, 6]

def f(l, callback): # Велосипед, есть filter
    result = []
    for i in l:
        if callback(i):
            result.append(i)
    return result

def odd(value):
    return bool(value % 2)

def even(value):
    return not value % 2

print(f(lst, odd))

print(f(lst, even))

lst2 = f(lst, lambda value: not value % 2)

lst3 = list(filter(lambda v: bool(v % 2), lst))
print(lst3)

lst4 = list(map(lambda v: str(v), lst))
lst4 = list(map(str, lst))
print(lst4)


"""
todo: Рекурсивная функция
1. Прямая
2. Косвенная

def a():
    b()

def b():
    a()

a()
"""

def factorial(n):
    """5! = 1 * 2 * 3 * 4 * 5"""
    return 1 if n == 0 else factorial(n - 1) * n

print(factorial(5))


# todo: Замыкания

def trim(chars=None):
    # замкнутая область видимости
    def f(s):
        return s.strip(chars)
    return f

trim_spaces = trim()
trim_slashes = trim('/\\|')
print(trim_spaces, type(trim_spaces))

s = '     text '
print(trim_spaces(s))

s = '/////|||||post\\\\/'
print(trim_slashes(s))


# todo: области видимости

g = 666
l_g = []

def wrapper():
    external = 777
    local_l = []

    def func():
        global g #!!!!!!!!!!!
        g += 1

        nonlocal external
        external += 1

        l_g.append(1)
        local_l.append(2)

    func()

    print(g, external, l_g, local_l)

wrapper()











