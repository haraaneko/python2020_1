# Однострочный комментарий

'''
Многострочный
комментарий
'''

"""
Многострочный
комментарий
"""

"""
todo: Как объявить переменную в Python?

PEP-8
"""

product_id = 1    # запись (присвоение)
print(product_id) # чтение

"""
todo: Какие типы данных есть в Python?

Тип данных, это характеристика и отражает она следующее:
- количество выделяемой памяти
- формат представления данных
- диапазон допустимых значений
- операции, допустимые для применения к переменной

1. Скалярные (простые):     bool int float complex str bytes
2. Ссылочные (структурные): tuple list set dict object
3. Специальные:             None

1. Неизменяемые (immutable): скалярные + tuple
2. Изменяемые (mutable):     ссылочные, кроме tuple
"""

# todo: bool - Логический тип

is_developer = True
has_id = False


# todo: int - Целочисленный тип

i1 = 123
i2 = 0b100
i3 = 0o755
i4 = 0x12FA
i5 = 0xffffff
i6 = -123
i7 = -0b100


# todo: float - Вещественный тип

f1 = 123.456
f2 = 1e6     # 1 * 10^6   => 1000000.0
f3 = 12e-3   # 12 * 10^-3 => 0.012
f4 = .1


"""
todo: complex - комплексные числа
a + bi
"""

c1 = complex(2, 3) # 2+3j
c2 = 2+3j

c3 = 3.14j     # 0+3.14j => complex(0, 3.14)
c3.real        # действительную часть
c3.imag        # мнимая часть
c3.conjugate() # сопряженное число


# todo: str - Строки

s1 = 'String'
s2 = "String"
s3 = "Hello, \"Linus\"!"
s4 = 'Hello, "Linus"!'
s5 = '\t123\n\t456'
s6 = '''
    '123'
    "456"
'''
s7 = """
Многострочный
текст
    с сохранением форматирования
"""
# print(s6)

s8 = r'^\d+$' # raw => сырые строки


# todo: bytes - байтовые строки

b1 = b'Hello'
b2 = bytes('Привет', 'utf-8')



# todo: tuple - Кортежи

t1 = ("Linus Torvalds", 50, True, ['C++', 'Linux'])
print(t1[2], t1[3][1])
# t1[2] = False
t1[3][1] = 'Windows'
print(t1[3][1])


# todo: list - Списки

l1 = ["Linus Torvalds", 50, True, ('C++', 'Linux')]
print(l1[2], l1[3][1])
l1[2] = False
print(l1[2])


# todo: set - множества

ss1 = {1, 2, 3, 1, 2, 3}
ss2 = {1, 7, 8}
ss1.add(4)
ss1.update(ss2)
ss1.update([0])
ss1.remove(3)
print(ss1)

empty_set = set() # пустое множество


# todo: dict - словари

d = {} # пустой словарь
person = {
    'name': 'Linus Torvalds',
    'age': 50,
    'is_developer': True,
    'skills': ('C++', 'Linux'),
}
print(person['age'], person['skills'][1])

persons = {
    'uid1': {},
    'uid2': {},
}


# None - Пустота

var = None


# todo: Как определить тип данных переменной?

print(
    type(var),
    type(persons),
    type(i1),
    type(f1),
    sep='\n'
)

# todo: Как выполнить явное приведение переменной
# к определенному типу?

"""
bool(x)
int(x [, base])
float(x)
complex(real, imag)
str(x)
bytes(x, encoding)
tuple(x)
list(x)
set(x)
dict(x)
"""

"""
todo: Какие операторы существуют в Python?

Арифметические:  + - * / % ** //
Сравнения:       == != > < >= <=
Присваивания:    = += -= *= /= %= **= //=
Логические:      and or not
Побитовые:       & | ~ ^ << >>
Принадлежности:  in, not in
Тождественности: is, not is (только для ссылочных типов)
"""
