# todo: Исключения

try:
    a = input()

    if a.isdigit():
        a = int(a)

    b = int(input())

    print(a + b)
except ValueError: # (ValueError, TypeError)
    print('Вы ввели не число')
except TypeError as err:
    print(err)
except:
    print('Какая ошибка')
else:
    print('Выполняется, если нет исключений')
finally:
    print('Выполняется всегда')


try:
    int(input()) + input()
except (TypeError, ValueError) as err:
    print(err)









