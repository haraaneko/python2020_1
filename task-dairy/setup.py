"""
name:    имя пакета
version: версия пакета

description:  краткое описание пакета
url:          URL-адрес пакета
license:      лицензия
author:       автор пакета
author_email: почта автора

packages:   пакеты, которые
            нужно скопировать при установке
            (без рекурсии)
py_modules: модули, которые
            нужно скопировать при установке
scripts:    запускаемые из консоли команды
install_requires: прямые зависимости пакета
"""

from setuptools import setup

setup(
    name='task-diary',
    version='0.0.0',
    description='Console diary.',
    license='Apache License 2.0',
    author='Kirill Vercetti',
    author_email='office@kyzima-spb.com',
    packages=['task_dairy']
)
