"""
Форматы данных
"""

import pickle
import json
import csv


data = {
    'users': [
        {
            'id': 1,
            'name': 'Linus Torvalds',
            'is_developer': True,
            'skills': ('C++', 'Linux'),
        },
        {
            'id': 2,
            'name': 'Richard Stallman',
            'is_developer': True,
            'skills': ('GNU', 'C'),
        },
    ],
}

# todo: Pickle

with open('users.pickle', 'wb') as f:
    pickle.dump(data, f)


with open('users.pickle', 'rb') as f:
    loaded_pickle = pickle.load(f)
    print(f'{loaded_pickle}\n')


# todo: JSON - JavaScript Object Notation

with open('users.json', 'w') as f:
    json.dump(data, f, indent=4)


with open('users.json') as f:
    loaded_json = json.load(f)
    print(f'{loaded_json}\n')


"""todo: CSV

id;name;is_developer;skills
1;Linus Torvalds;1;C++,Linux
2;Richard Stallman;1;GNU,C
"""

with open('users.csv', 'w') as f:
    users = data.get('users', [])

    if users:
        fieldnames = users[0].keys()
        writer = csv.DictWriter(f, fieldnames=fieldnames)
        writer.writeheader()
        writer.writerows(users)


with open('users.csv') as f:
    reader = csv.DictReader(f)
    loaded_csv = list(reader)
    print(f'{loaded_csv}\n')


# todo: YAML (yml) py-yaml

"""todo: XML

lxml

XML  -> XPath
HTML -> CSS selectors

<users>
    <user>
        <id>1</id>
        <name>Linus Torvalds</name>
        <skills>
            <skill>C++</skill>
        </skills>
    </user>
    <user id="2" name="Richard Stallman" is_developer>
        <skills>
            <skill value="C">
            <skill value="GNU">
        </skills>
    </user>
</users>
"""

# todo: INI - Конфигурационные файлы configparser
"""
[db]
host "localhost"
user "root"

[main]
debug on
"""




